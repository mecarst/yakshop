class ApiError extends Error {
    json() {
        console.log(`${new Date().toLocaleString()}, ${this.status}:
            ${this.originalError ? this.originalError.message : this.message}`);
        
        return {
            error: this.message
        };
    }
}

exports.RequiredField = class RequiredField extends ApiError {
    constructor(...fields) {
        super();
        this.status = 400;
        this.message = `Required fields: ${fields.join(', ')}`;
    }
};