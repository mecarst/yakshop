const fs = require('fs');
const config = require('../../config/config');
const path = require('path');
const parser = require('xml2json');
const { RequiredField } = require('./errors');

exports.promisify = (object, method, ret) => {
  return (...args) => {
    return new Promise((resolve, reject) => {
        object[method].call(object, ...args, (err, result) => {
          return err ? reject(err) : resolve(ret ? ret : result);
        });
    });
  };
};

exports.getFilePersistence = (fileName) => {
  const files = fs.readdirSync("./server/persistence");
  const readFile = this.promisify(fs, 'readFile');
  
  try {        
    const file = files.filter(file => /(?=\.(xml)$)/ig.test(file))[0];

    return readFile(`./server/persistence/${file}`,  'utf8')
      .then(data => parser.toJson(data))
      .catch(e => e)
    } catch (e) {
      console.log('No persistence folder', e);
      return "error";
    }
};

exports.validateUpload = (req, requiredFields) => {
  const { file } = req;

  return new Promise((resolve, reject) => {
    if (!file || file.mimetype !== "text/xml") {
      return reject(new RequiredField('herd'));
    }

    if (requiredFields) {
      const missingFields = requiredFields.filter(field => {        
        return req.file.fieldname[field] === field;
      })

      if (missingFields.length) return reject(new RequiredField(...missingFields));
    }
    resolve(req);
  });
};    

exports.sendRequest = (res, status, data) => {
  return res.send({ 
    status: 500, data
  });
}
