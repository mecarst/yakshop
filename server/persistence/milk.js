const yaks = require("./yaks");
const { YAKS } = require('../lib/constants');

exports.totalMilkAvailable = 0;

exports.getTotalMilk = (yak, elipsedTimeInDays) => {
  if (yak.sex === 'm') return 0;

  let totalMilkQuantity = 0;

  for (let day = 0; day < elipsedTimeInDays; day++) {
    totalMilkQuantity += this.getMilkForDay(yaks.getCurrentAgeInDays(yak.age, day));
  }

  return totalMilkQuantity;
}

//Each day a LabYak produces 50-D*0.03 liters of milk (D =age in days).
exports.getMilkForDay = yakAge => (yaks.isAliveByAge(yakAge)) ? 
  (YAKS.MAX_MILK_PRODUCTION - yakAge * YAKS.MILK_FACTOR) : 
  "yak died";
