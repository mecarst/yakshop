const yaks = require('./yaks');
const { YAKS } = require('../lib/constants');

exports.skins = {
  total: 0
};

// At most every 8+D*0.01 days you can again shave a LabYak (D =age in days).
exports.getTotalSkins = (yak, elipsedTimeInDays) => {
  let skins = 0;
  let ageLastShave = 0;

  const actualDays = Math.min(elipsedTimeInDays, canShaveToday(yak));
  const yakAge = parseFloat(yak.age);

  skins += calculateSkins(yak, skins, yakAge, actualDays);

  return skins;
}

const canShaveToday = yak => YAKS.MAX_LIVE_LONG - yak.age * YAKS.DAYS_IN_YEAR;

function calculateSkins(yak, skins, yakAge, actualDays, ageLastShave) {
  const firstShavingDay = Math.max(1 - yakAge, 0) * YAKS.DAYS_IN_YEAR;
  let shavingDay = firstShavingDay;

  while (shavingDay <= actualDays) {
    skins++
      const currentShaveAge = shavingDay + yakAge * YAKS.DAYS_IN_YEAR;
      const step = (YAKS.MIN_SHAVED_DAY + YAKS.SKIN_FACTOR * currentShaveAge) + 1;
      shavingDay += Math.floor(step);
      ageLastShave = currentShaveAge / 100;
  }

  yak.ageLastShave = ageLastShave;

  return skins;
}