const { getTotalSkins, totalSkinsAvailable, getWoolSkinCount } = require('./skins');
const { getTotalMilk, totalMilkAvailable } = require('./milk');
const { YAKS } = require('../lib/constants');

exports.yaksList = [];

exports.calculateYaksSkinsAndMilk = (elipsedTimeInDays, getAge = false) => {
  return new Promise((res, rej) => {

    let totalMilkQuantity = 0;
    let totalSkinsQuantity = 0;

    this.yaksList[0].forEach(yak => {
      totalMilkQuantity += getTotalMilk(yak, elipsedTimeInDays);
      totalSkinsQuantity += getTotalSkins(yak, elipsedTimeInDays, getAge);
    });

    if (typeof totalMilkQuantity !== "number") {
      return rej("Error: could not get total ammount for yaks")
    }

    return res({milk: totalMilkQuantity, skins:totalSkinsQuantity});
  });
}

exports.getCurrentAgeInDays = (yakAge, elapsedDays) => {
  return ((parseFloat(yakAge) * YAKS.DAYS_IN_YEAR + parseFloat(elapsedDays)));
}

exports.isAliveByAge = (ageInDays) => {
  return ageInDays < 1000;
}

exports.isYakAlive = (yakAge, ageInDays) => {
  return this.getCurrentAgeInDays(yakAge, ageInDays) < 1000;
}

 exports.calculateCurrentAgeInDays = (elapsedTimeInDays) => {
  return (yak.age * YAKS.DAYS_IN_YEAR + elapsedTimeInDays);
}

