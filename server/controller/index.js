const herd = require('./herd');
const order = require('./order');
const stock = require('./stock');

module.exports = { herd, order, stock }