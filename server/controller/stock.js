const fs = require('fs');
const path = require('path');
const { sendRequest } = require('../lib/utils');
const { validateUpload, promisify } = require('../lib/utils');
const { yaksList, calculateYaksSkinsAndMilk} = require('../persistence/yaks');
const { ordered } = require('./order');

exports.getStock = (req, res, next) => {
	const {t} = req.params;

  //TO DO make sure the CALCULATE is correct
  return calculateYaksSkinsAndMilk(t)
    .then(yaks => sendRequest(res, 200, {data: {
      "milk:" : yaks.milk,
      "skins:": yaks.skins
    }}))
    .catch(err => sendRequest(res, 500, {data: `Error: ${err}`}));
};

exports.loadNewStock = (req, res, next) => {
  const requiredFields = ['herd'];

  return  validateUploadFile(req)
    .then(file => saveFile(file))
    .then(data => sendRequest(res, 205, {data: `You uploaded ${data.originalname}`}))
    .catch(e => sendRequest(res, e.status, {err: e.message}));
}

function validateUploadFile ({body, file}) {
  const xmlTxtType = 'text/xml';
  const validExtensions = ['xml'];

  return new Promise((resolve, reject) => (
    (validExtensions.find(ext => ~file.originalname.toLowerCase().indexOf(ext))) ? 
      resolve(file) : 
      reject(file)
  ));
};

function saveFile(herdFile) {
  const writeFile = promisify(fs, 'writeFile');
  const persistence = path.resolve('./server/persistence')
  const initialPath = path.join(`${persistence}/${herdFile.originalname}`);

  return writeFile(
      `${initialPath}`, 
      herdFile.buffer, 
      { encoding:'utf8', flag:'w' }
    )
    .then(() => herdFile)
    .catch(err => err);
}
