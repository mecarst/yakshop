const { yaksList, calculateYaksSkinsAndMilk} = require('../persistence/yaks');
const { sendRequest } = require('../lib/utils');

exports.orders = [];
let orderedFromShop = {
  milk: 0,
  skins: 0,
};

exports.placeOrder = (req, res, next) => {
  const { body, params: {t} } = req;

  if (!body.order) return sendRequest(res, 500, {err: "Please place an order"});

  return calcOrder(body, t)
    .then(order => checkOrderData(res, order, body.order))
    .catch(err => sendRequest(req, 500, {err: "Could not place order"}));
};

exports.getOrders = (req, res, next) => sendRequest(res, 200, {orders: this.orders});

const calcOrder = (req, elipsedTimeinDays) => {
  const { order } = req;

  return calculateYaksSkinsAndMilk(elipsedTimeinDays, true)
    .then(data => {

      let placedOrder = {
        customer: req.customer,
      };
      placedOrder['milk'] = parseFloat(order.milk) <= parseFloat(data.milk) ? parseFloat(order.milk) : -1;
      placedOrder['skins'] = parseInt(order.skins) <= parseInt(data.skins) ? parseInt(order.skins) : -1;

      this.orders.push(placedOrder);
      orderedFromShop.milk += parseFloat(order.milk) === -1 ? 0 : parseFloat(order.milk);
      orderedFromShop.skins += parseInt(order.skins) === -1 ? 0 : parseInt(order.skins);

      return placedOrder;
    })
    .catch(err => err)
}

function checkOrderData(res, order, placedOrder) {
  if (order.milk == -1 && order.skins == -1 || order.milk == -1 && order.skins == 0 || order.milk == 0 && order.skins == -1) {
    return sendRequest(res, 404, {err: "Couldn't palce order"});
  } else if (order.milk == placedOrder.milk && order.skins == placedOrder.skins) {
    return sendRequest(res, 201, {data: order});
  } else {
    return sendRequest(res, 206, {data: order});
  } 
}