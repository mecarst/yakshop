const fs = require('fs');
const config = require('../../config/config');
const { yaksList, calculateYaksSkinsAndMilk, getAgeLastShave } = require('../persistence/yaks');
const { sendRequest } = require('../lib/utils');


exports.getHerdData = (req, res, next) => {
  const {t} = req.params;

  if (!yaksList && yaksList[0].length <= 0) {
    return sendRequest(res, 500, {err: "Error: couldn't find any yaks."})
  }

  if (!t) {
    return sendRequest(res, 404, {err: "Error, time in days not specified"});
  }
  
  return calculateYaksSkinsAndMilk(t)
    .then(yaks => {
      yaksList[0].map(yak => delete yak.ageLastShave);
      return sendRequest(res, 200, {
        herd: { 
          "InStock": `${yaks.milk} liters of milk - ${yaks.skins} skins of wool`,
          "Herd": yaksList[0]
      }});
    })
    .catch(err => sendRequest(res, 500, {data: `Error: ${err}`}));
};

//TO DO CALCULATE THE HERD
exports.getHerd = (req, res, next) => {
  const {t} = req.params; 

  return calculateYaksSkinsAndMilk(t, true)
    .then(data => sendRequest(res, 200, {herd: yaksList[0]}))
    .catch(err => sendRequest(res, 500, {error: `Error getting the herd ${err}`}));
}

