const { getFilePersistence} = require('../lib/utils');
const { yaksList} = require('../persistence/yaks');

module.exports = initApp => {
  const agetLastShave = 0;

  return getFilePersistence('herd')
    .then(data => {
      const { labyak } = JSON.parse(data).herd;
      labyak.forEach(yak => yak.ageLastShave = 0)
      yaksList.push(labyak);
    })
    .catch(e => e);
}

