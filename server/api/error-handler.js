exports.uploadErrorHandler = (err, req, res, next) => {
    if (err) {
        const error = `${err.message}: ${err.field}`;
        return res.status(error.status || 500).json(error);
    }
    next();
};
