const router = require('express').Router();
const multer = require('multer');
const upload = multer({
  storage: multer.memoryStorage()
});

const { uploadErrorHandler, notPermittedError } = require('./error-handler');
const YaksController = require('../controller/index');
const {
  herd:   { getHerdData, getHerd },
  stock:  { getStock, loadNewStock },
  order:  { getOrders, placeOrder }
} = YaksController;

module.exports =  router
  //story 1
  .get('/herd/:t', getHerdData)
  //story 2
  .post('/yak-shop/load', upload.single('newHerd'), uploadErrorHandler, loadNewStock)
  //story 3
  .get('/yak-shop/stock/:t', getStock)
  .get('/yak-shop/herd/:t', getHerd)
  //story 4
  .post('/yak-shop/order/:t', placeOrder)
  .get('/yak-shop/orders', getOrders);
  
  
  
