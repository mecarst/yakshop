'use strict';

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const api = require('./api');
const app = express();
const init = require('./init/init');

init();

app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
	.use(bodyParser.json({ limit: '10mb' }))
	.use((err, req, res, next) => res.status(500).send(err.message))
  .use(cors())
  .use(express.static(path.join(__dirname, '../dist/yakshop')))  
  .use('/', api)
  .get('/', (req, res) => res.sendFile(path.join(__dirname, '../dist/yakshop/index.html')));

module.exports = app;
