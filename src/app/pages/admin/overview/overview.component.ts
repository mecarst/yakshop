import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { YakShopService } from  '../../../services/yak.service';

@Component({
  selector: 'yak-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  private overview: any;
  herd: any;
  milk: number = 0;
  skins: number = 0;
  stock: any;

  submited: boolean = false;
  error: boolean = false;
  fileExtension: boolean = true;
  herdFile: any;
  fileName: string;

  newHerd = new FormControl('', [Validators.required]);
  herdForm: FormGroup = this.formBuilder.group({
    newHerd: this.newHerd
  });

  constructor(
    private yakApiService: YakShopService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.getHerdData(14);
  }

  private getHerdData(timeInDays: number) {
    this.yakApiService
      .getHerdData(timeInDays)
      .subscribe((data) => {
        if (data && data['data'].hasOwnProperty('herd')) {
          this.overview = data['data'].herd;
          this.getStockInfo();
        }
      });
  }

  private getStockInfo() {
    const { InStock, Herd } = this.overview;
    this.milk = InStock.split("-")[0];
    this.skins = InStock.split("-")[1]
    this.herd = Herd;
  }

  submitHerd(event: Event) {
    const { newHerd } = this.herdForm.value;
    const fileExt = '.xml';

    if (newHerd.indexOf(fileExt) <= 0) {
      this.fileExtension = false;
      return false;
    }

    this.yakApiService
      .uploadHerd(event.target[0].files)
      .subscribe(
        res => {
          if (res && res.hasOwnProperty('data')) {
            this.showHerd(event.target[0].files);
          }
        },
        err => {
          console.error(err);
          this.error = true;
        }
      );
  }

  showHerd(file) {
    this.submited = true;
    this.fileName = file[0].name;
  }
}
