import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OverviewRoutingModule } from './overview-routing.module';
import { OverviewComponent } from './overview.component';

@NgModule({
  imports: [
    CommonModule,
    OverviewRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [OverviewComponent]
})
export class OverviewModule { }
