import { Component, OnInit } from '@angular/core';
import { YakShopService } from  '../../../services/yak.service';

@Component({
  selector: 'yak-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders: any;

  constructor(private yakApiService: YakShopService,) { }

  ngOnInit() {
    this.getOrdersData();
  }

  private getOrdersData() {
    console.log("askldjalksdjalsd")
    this.yakApiService
      .getOrders()
      .subscribe((data) => {
        if (data && data['data'].hasOwnProperty('orders')) {
          this.orders = data['data'].orders;
        }
      });
  }

}
