import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from "@angular/router";

@Component({
  selector: 'yak-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  submited: boolean = false;
  error: boolean = false;

  username = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);

  loginForm: FormGroup = this.formBuilder.group({
    username: this.username,
    password: this.password
  });

  private loginUser: string = "admin";
  private loginPass: string = "admin";

  constructor(
    private formBuilder: FormBuilder, 
    private router: Router
  ) { }

  ngOnInit() {
  }

  login(event: Event) {
    const {username, password } = this.loginForm.value
    if (username === "" || password === "") return false;
    if (username !== this.loginUser && password !== this.loginPass) return false;

    this.router.navigate(['/admin/overview']);
  }

}
