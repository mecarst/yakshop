import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { YakShopService } from  '../../services/yak.service';

@Component({
  selector: 'yak-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {  
  deliveryMinDate: any = Date.now();
  deliveryMaxDate: any = new Date("09-31-2018");
  orderDate: any = 1;
  placedOrder: any;
  submited: boolean = false;
  error: boolean = false;
  milkOrdered: number = 0;
  skinsOrdered: number = 0;
  milkOutOfStock: string;
  skinsOutOfStock: string;

  username = new FormControl('', [Validators.required]);
  milk = new FormControl('', [Validators.required, Validators.maxLength(3)]);
  skins = new FormControl('', [Validators.required, Validators.maxLength(3)]);
  date = new FormControl();

  orderForm: FormGroup = this.formBuilder.group({
    username: this.username,
    milk: this.milk,
    skins: this.skins,
    date: this.date
  });

  constructor(
    private formBuilder: FormBuilder,
    private yakApiService: YakShopService
  ) {}

  ngOnInit() {}

  submitOrder(event: Event) {
    const { username, milk, skins, date } = this.orderForm.value;
    if (username === "" || milk === "" || skins === "") return false;

    this.orderDate = this.daysBetween(this.deliveryMinDate, new Date(date));
    this.placeOrder(this.orderForm.value);
  }

  placeOrder({username, milk, skins}) {
    const order = {
      customer : username, 
      order : {
        milk : parseInt(milk),
        skins : parseInt(skins)
      }
    }

    this.yakApiService
      .placeOrder(order, this.orderDate)
      .subscribe(
        res => {
          if (res && res.hasOwnProperty('data')) {
            this.placedOrder = res['data'];
            this.showOrderRes();
          }
        },
        err => {
          console.error(err);
          this.error = true;
        }
      );
  }

  daysBetween(minDate, pickedDate ) {
    const one_day = 1000 * 60 * 60 * 24;
    const difference_ms = minDate - pickedDate;
    return Math.abs(Math.round(difference_ms/one_day)); 
  }

  showOrderRes() {
    this.submited = true;

    const {milk, skins} = this.placedOrder.data;

    this.milkOrdered = milk;
    this.skinsOrdered = skins;

    if (this.milkOrdered <= 0) {
      this.milkOutOfStock = "We are out of stock for milk!";
      this.milkOrdered = 0;
    }

    if (this.skinsOrdered <= 0) {
      this.skinsOutOfStock =  "We are out of stock for wool!";
      this.skinsOrdered = 0;
    }
  }

}




