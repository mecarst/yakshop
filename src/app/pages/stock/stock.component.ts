import { Component, OnInit } from '@angular/core';
import { YakShopService } from  '../../services/yak.service';

@Component({
  selector: 'yak-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {
  private herd: any = {};
  milk: number = 0;
  skins: number = 0;
  stock: any

  constructor(private yakApiService: YakShopService) {}

  ngOnInit() {
    this.getHerdData(14);
  }

  private getHerdData(timeInDays: number) {
    this.yakApiService
      .getHerdData(timeInDays)
      .subscribe((data:  Array<object>) => {
        if (data && data['data'].hasOwnProperty('herd')) {
          this.herd = data['data'].herd;
          this.getStockInfo();
        }
      });
  }

  private getStockInfo() {
    const {InStock} = this.herd;
    this.milk = InStock.split("-")[0];
    this.skins = InStock.split("-")[1]
  }
}
