import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from './pages/not-found/not-found.component';

// import { HerdComponent } from './pages/admin/herd/herd.component';

const appRoutes: Routes = [
  { path: '', redirectTo: "/home", pathMatch: "full" },
  { path: 'home', loadChildren: './pages/home/home.module#HomeModule' },  
  { path: 'stock', loadChildren: './pages/stock/stock.module#StockModule' },
  { path: 'admin', loadChildren: './pages/admin/admin.module#AdminModule'},
  { path: 'orders', loadChildren: './pages/orders/orders.module#OrdersModule' },  
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
