import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';


import { environment } from '../../environments/environment';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class YakShopService {
  constructor(private httpClient: HttpClient) {}

  //GET '/herd/:t'
  public getHerdData(elipsedTimeInDays: number = 14 ) {
    return this.httpClient.get(`${API_URL}/herd/${elipsedTimeInDays}`);
  }

  //POST '/yak-shop/load'
  public uploadHerd(herdFile: File) {
    const uploadData = new FormData();
    uploadData.append('newHerd', herdFile[0]);

    return this.httpClient.post(`${API_URL}/yak-shop/load`, uploadData);
  }

  //GET '/yak-shop/stock/:t'
  public getStock() {}

  //GET '/yak-shop/herd/:t
  public getHerd() {}

  //POST '/yak-shop/order/:t
  public placeOrder(order, orderDate) {
    return this.httpClient.post(`${API_URL}/yak-shop/order/${orderDate}`, order);
  }

  //GET '/yak-shop/orders'
  public getOrders() {
    return this.httpClient.get(`${API_URL}/yak-shop/orders`);
  }

}
