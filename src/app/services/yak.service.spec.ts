import { TestBed, inject } from '@angular/core/testing';

import { YakShopService } from './yak.service';

describe('YakShopService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YakShopService]
    });
  });

  it('should be created', inject([YakShopService], (service: YakShopService) => {
    expect(service).toBeTruthy();
  }));
});
