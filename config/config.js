const path = require('path');

module.exports = {
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 8082,
    herdFilePath: process.env.FONT_PATH || path.resolve('./server/persistence/')
};
